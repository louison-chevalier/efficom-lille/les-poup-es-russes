<header class="header">
    <div class="mdc-layout-grid">
        <div class="mdc-layout-grid__inner">
            <div class="mdc-layout-grid__cell--span-12-desktop mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-4-phone">
                <div class="header-commun">
                    <div class="header-commun__logo">
                        <img src="{{ asset('img/LogoPoupeesRusse.svg') }}" alt="LogoPoupeesRusse">
                    </div>
                    <div class="header-commun__nav">
                        <ul>
                            <li><a href="">T-SHIRT & TOPS</a></li>
                            <li><a href="">BODY</a></li>
                            <li><a href="">PULL</a></li>
                            <li><a href="">ROBES & COMBIS</a></li>
                            <li><a href="">JUPES & SHORTS</a></li>
                            <li><a href="">JEANS & PANTALON</a></li>
                            <li><a href="">VESTES</a></li>
                            <li><a href="">CHAUSSURES</a></li>
                            <li><a href="">ACCESSOIRES</a></li>
                            <li><a href="">BIJOUX</a></li>
                        </ul>
                    </div>
                    <div class="header-commun__market">
                        <button class="btn-market">
                            <i class=" mdi mdi-cart-outline"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
