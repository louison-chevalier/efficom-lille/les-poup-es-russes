<footer class="footer">
    <div class="mdc-layout-grid">
        <div class="mdc-layout-grid__inner">
            <div
                class="mdc-layout-grid__cell--span-12-desktop mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-4-phone">
                <div class="footer-commun">

                    <!--LEFT-->
                    <div class="footer-commun__left">
                        <div class="footer-list">
                            <ul>
                                <li><a href="#">T-SHIRT & TOPS</a></li>
                                <li><a href="#">BODY</a></li>
                                <li><a href="#">PULL</a></li>
                                <li><a href="#">ROBES & COMBIS</a></li>
                                <li><a href="#">JUPES & SHORTS</a></li>
                                <li><a href="#">JEANS & PANTALON</a></li>
                                <li><a href="#">VESTES</a></li>
                                <li><a href="#">CHAUSSURES</a></li>
                                <li><a href="#">ACCESOIRES</a></li>
                                <li><a href="#">BIJOUX</a></li>
                            </ul>
                        </div>
                        <div class="footer-list">
                            <ul class="footer-list--grey">
                                <li><a href="#">LA MARQUE</a></li>
                                <li><a href="#">ACTUALITÉS</a></li>
                                <li><a href="#">FAQs</a></li>
                                <li><a href="#">GUIDE DES TAILLES</a></li>
                                <li><a href="#">LE TEXTILE</a></li>
                                <li><a href="#">LIVRAISON & RETOURS</a></li>
                                <li><a href="#">CONDITIONS GÉNÉRALES DE VENTES</a></li>
                                <li><a href="#">MENTIONS LÉGALES</a></li>
                                <li><a href="#">PROTECTION DES DONNÉES</a></li>
                                <li><a href="#">CONTACT</a></li>
                            </ul>
                        </div>
                    </div>

                    <!--RIGHT-->
                    <div class="footer-commun__right">
                        <div class="footer-commun__right--align">
                            <div class="footer-right footer-right__logo">
                                <img src="{{ asset('img/LogoGrey.svg') }}" alt="LogoPoupeesRusse">
                            </div>
                            <div class="footer-right footer-right__socials">
                                <span><i class="mdi mdi-facebook"></i></span>
                                <span><i class="mdi mdi-instagram"></i></span>
                            </div>
                            <div class="footer-right footer-right__localisation">
                                <p>27 rue du mont de Resquillon <br>
                                    59251 Allènes les marais France <br>
                                    boutiquelespoupeesrusses@gmail.com
                                </p>
                            </div>
                            <div class="footer-right footer-right__copyright">
                                <p>TradeMarks© 2019, all rights reserved.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</footer>
