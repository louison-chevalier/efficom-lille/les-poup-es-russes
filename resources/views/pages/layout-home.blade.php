@section('content')
@extends('index')

<section class="home">
    <div class="mdc-layout-grid">
        <div class="mdc-layout-grid__inner">
            <div class="home-content mdc-layout-grid__cell--span-12-desktop mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-4-phone">
                <!--BOX-->
                @include('pages.home.home')

            </div>
        </div>
    </div>
</section>

@stop
