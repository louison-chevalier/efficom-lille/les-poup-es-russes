<section class="box">
    <div class="box-content">
        <!--TOP BOX-->
        <div class="box-content__top">
            <div class="box-content__top-left" style="background-image: url('{{  asset('img/Img_homepage.png') }}');">
                <h2>Boutique en ligne de vêtements féminins</h2>
            </div>
            <div class="box-content__top-right">
                <div class="box-content__top-right__text">
                    <p>Profitez de vêtements, chaussures, bijoux et
                        accessoires à des prix intéressants.
                        Shoppez dès maintenant votre look tendance sur
                        notre catalogue en ligne, pour tous les styles,
                        toutes les saisons, toutes les situations… </p>
                </div>
                <div class="box-content__top-right__socials">
                    <div class="socials-home socials-facebook">
                        <i class="mdi mdi-facebook"></i>
                    </div>
                    <div class="socials-home socials-instagram">
                        <i class="mdi mdi-instagram"></i>
                    </div>
                </div>
            </div>
        </div>
        <!--BOTTOM BOX-->
        <div class="box-content__bottom">
            <div class="box-content__bottom-left">
                <div class="box-content__bottom-left bottom-top">
                    <div class="home-trigger">
                        <span>SUIVANT</span>
                        <span>La marque</span>
                    </div>
                    <div class="home-arrow">
                        <span class='the-arrow -right'>
                            <span class='shaft'></span>
                        </span>
                        <span class='the-arrow -left'>
                            <span class='shaft'></span>
                        </span>
                    </div>
                </div>
                <div class="box-content__bottom-left bottom-bot"
                    style="background-image: url('{{ asset('img/bijoux.png') }}');">
                </div>
            </div>
            <div class="box-content__bottom-right" style="background-image: url('{{ asset('img/img_autumn.png') }}');">
                <h2>Nouvelle collection d’automne 2019-2020</h2>
                <div class="home-list">
                    <div class="home-list__img"
                        style="background-image: url('{{ asset('img/article_autumn_1.png') }}');"></div>
                    <div class="home-list__img"
                        style="background-image: url('{{ asset('img/article_autumn_2.png') }}');"></div>
                    <div class="home-list__img">Voir plus <i class="mdi mdi-chevron-right"></i> </div>
                </div>
            </div>
        </div>
    </div>
</section>
