@section('content')
@extends('index')

<section class="contact">
    <div class="mdc-layout-grid">
        <div class="mdc-layout-grid__inner">
            <div class="mdc-layout-grid__cell--span-6-desktop mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-4-phone">
                <div class="contact-left" data-aos="fade-up" data-aos-duration="1000">
                    <div class="reveal">
                        <img class="contact-left--img" data-scroll src="{{ asset('img/mail.jpg') }}">
                    </div>
                </div>
            </div>
            <div class="mdc-layout-grid__cell--span-6-desktop mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-4-phone">
                <div class="contact-right">
                    <h2 class="splitting-word subtitle" data-scroll data-splitting>T'aimes bien mon travail ? Tu veux discuter ou <br> bien me raconter ta vie ?</h2>
                    <h1 class="splitting-char title--presentation" data-scroll data-splitting>Tu es au bon<br> endroit mon pote.</h1>
                    <div class="contact-right__form">
                        <!-- FORM -->
                        <form action="/mail" method="POST">
                            {{ csrf_field() }}
                            <div class="container">
                                <div class="filds">
                                    <input type="text" name="nom" placeholder="Nom">
                                    <label for="firstName">Ton nom.</label>
                                </div>
                                <div class="filds">
                                    <input type="text" name="mail" placeholder="Mail">
                                    <label for="password">Ton mail.</label>
                                </div>
                                <div class="filds">
                                    <textarea name="text"  row="6" cols="40" placeholder="Votre message."></textarea>
                                    <label for="teaxarea">Déguène ta plume et écrit moi ta plus belle poésie..</label>
                                </div>
                                <div class="filds">
                                    <button type="submit"><span class="mdi mdi-email-outline"></span>&nbsp;&nbsp;&nbsp;Appuie sur le button pour me l'envoyer.</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>

@stop
