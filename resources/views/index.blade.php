<html lang="{{app()->getLocale()}}">

<head>
    <title>Poupees</title>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5, user-scalable=0">
    <meta name="description" content="test">
    <link rel="icon" href="{{asset('img/favicon.png')}}" type="image/x-icon" />

    <!-- StyleSheets -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

    <!--Import Google Icon Font-->
    <link rel="stylesheet" type="text/css"
        href="https://cdn.materialdesignicons.com/3.5.95/css/materialdesignicons.min.css" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{csrf_token()}}">
</head>

<body>
    <!-- OTHERS -->
    @include('component.loader')

    <!-- MAIN -->
    @include('main.header')

    <!-- FOOTER -->


    <!-- CONTENT -->
    @yield('content')

</body>

<!-- Script files -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="http://morphist.fyianlai.com/assets/js/morphist.js"></script>

<script type="text/javascript" src="{{asset('js/main.js')}}"></script>
