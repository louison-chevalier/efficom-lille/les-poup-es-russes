<html lang="{{app()->getLocale()}}">

<head>
    <meta charset="utf-8">
    <title>Les Poupées Russes</title>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5, user-scalable=0">
    <meta name="description" content="Pas de bras, pas de chocolat..">
    <meta name="author" content="Julien Fovelle">
    <link rel="icon" href="{{setting('site.logo')}}" type="image/x-icon" />


    <!-- StyleSheets -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

    <!--Import Google Icon Font-->
	<link rel="stylesheet" type="text/css" href="https://cdn.materialdesignicons.com/3.5.95/css/materialdesignicons.min.css"/>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{csrf_token()}}">
</head>



<body>
    <!-- OTHERS -->
    @include('component.loader')
    @include('component.cursor')

   <!-- ERROR 404-->

<section class="error">
        <div class="error__content">
            <div class="mdc-layout-grid">
                <div class="mdc-layout-grid__inner">
                    <div class="mdc-layout-grid__cell--span-12-desktop">
                        <h2 class="splitting-word subtitle" data-scroll data-splitting>"Faite demi-tour dès que possible"
                        </h2>
                        <h1 class="splitting-char title--presentation" data-scroll data-splitting>#403.</h1>
                        <a href="/">
                            <div class="arrow-animation">
                                <i class="arrow-animation__fill"></i>
                                <span>Retour à la maison</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- FOOTER -->
    @include('main.footer')
</body>

<!-- Script files -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="http://morphist.fyianlai.com/assets/js/morphist.js"></script>

<script type="text/javascript" src="{{asset('js/main.js')}}"></script>

