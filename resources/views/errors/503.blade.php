<html lang="{{app()->getLocale()}}">

<head>
    <meta charset="utf-8">
    <title>Julien Fovelle — ERROR 503</title>
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5, user-scalable=0">
    <meta name="description" content="Pas de bras, pas de chocolat..">
    <meta name="author" content="Julien Fovelle">
    <link rel="icon" href="{{setting('site.logo')}}" type="image/x-icon" />

    <!--Parameters-->
    <meta name="referrer" content="no-referrer-when-downgrade" />
    <meta name="robots" content="all" />
    <link rel="canonical" href="https://julienfovelle.fr/" />
    <link rel="manifest" href="/manifest.json">
    <meta name="geo.placename" content="Leforest" />

    <!-- StyleSheets -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">

    <!--Import Google Icon Font-->
    <link rel="stylesheet" type="text/css"
        href="https://cdn.materialdesignicons.com/3.5.95/css/materialdesignicons.min.css" />

    <!-- Meta SocialMedia -->
    <meta property="og:site_name" content="Julien Fovelle">
    <meta property="og:locale" content="fr_FR">
    <meta property="og:type" content="website" />
    <meta property="og:title" content="{{setting('site.title')}}" />
    <meta property="og:description" content="{{setting('site.description')}}">
    <meta property="og:url" content="https://julienfovelle.fr/" />
    <meta property="og:image" content="" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{csrf_token()}}">
</head>



<body>
    <!-- OTHERS -->
    @include('component.cookie')
    @include('component.loader')
    @include('component.cursor')

    <!-- ALL -->
    @include('component.menu')
    @include('main.aside')

    <!-- ERROR 503-->
    <section class="error">
        <div class="error__content">
            <div class="mdc-layout-grid">
                <div class="mdc-layout-grid__inner">
                    <div class="mdc-layout-grid__cell--span-12-desktop">
                        <h2 class="splitting-word subtitle" data-scroll data-splitting>Aie, c'est pas le bon chemin..
                        </h2>
                        <h1 class="splitting-char title--presentation" data-scroll data-splitting>#503.</h1>
                        <a href="/">
                            <div class="arrow-animation">
                                <i class="arrow-animation__fill"></i>
                                <span>Retour à la maison</span>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- FOOTER -->
    @include('main.footer')
</body>

<!-- Script files -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"
    integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="http://morphist.fyianlai.com/assets/js/morphist.js"></script>

<script type="text/javascript" src="{{asset('js/main.js')}}"></script>

<script type="application/ld+json">
    {
        "@context": "http://schema.org",
        "@type": "WebSite",
        "url": "https://www.julienfovelle.fr/",
        "name": "Name",
        "logo": "https://www.julienfovelle.fr/src/img/logo.png",
        "contactPoint": {
            "@type": "ContactPoint",
            "telephone": "+33 6 77 63 40 39,
            "contactType": "Julien Fovelle",
        },
        "publisher": {
            "@type": "Organization",
            "name": "Julien Fovelle",
            "url": "https://www.julienfovelle.fr/",
            "logo": {
                "@type": "ImageObject",
                "url": "https://www.julienfovelle.fr/ressources/img/logo.png"
            }
        }
    }

</script>
