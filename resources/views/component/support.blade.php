<header class="support">
    <div class="mdc-layout-grid">
        <div class="mdc-layout-grid__inner">
            <div
                class="support-card mdc-layout-grid__cell mdc-layout-grid__cell--span-3-desktop mdc-layout-grid__cell--span-4-tablet mdc-layout-grid__cell--span-4-phone">
                <img src="{{ asset('img/icons/locked.svg') }}" alt="lock">
                <span>PAIEMENT SÉCURISÉ</span>
                <p>Visa, Mastercard, Paypal, American Express.</p>
            </div>
            <div
                class="support-card mdc-layout-grid__cell mdc-layout-grid__cell--span-3-desktop mdc-layout-grid__cell--span-4-tablet mdc-layout-grid__cell--span-4-phone">
                <img src="{{ asset('img/icons/pphone.svg') }}" alt="phone">
                <span>+33 6 78 45 12 36</span>
                <p>Disponible 7/7j & 24/24h</p>
            </div>
            <div
                class="support-card mdc-layout-grid__cell mdc-layout-grid__cell--span-3-desktop mdc-layout-grid__cell--span-4-tablet mdc-layout-grid__cell--span-4-phone">
                <img src="{{ asset('img/icons/box.svg') }}" alt="box">
                <span>LIVRAISON  EN 48H </span>
                <p>En France métropolitaine et en Belgique sans minimum d'achat.</p>
            </div>
            <div
                class="support-card mdc-layout-grid__cell mdc-layout-grid__cell--span-3-desktop mdc-layout-grid__cell--span-4-tablet mdc-layout-grid__cell--span-4-phone">
                <img src="{{ asset('img/icons/delivery.svg') }}" alt="delivery">
                <span>Frais de retours</span>
                <p>Remboursement sous 7 jours après ouverture.</p>
            </div>
        </div>
    </div>
</header>

