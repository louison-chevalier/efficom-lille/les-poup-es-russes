import "splitting/dist/splitting.css";
import "splitting/dist/splitting-cells.css";
import Splitting from "splitting";

Splitting({
    target: "[data-splitting]",
    by: "chars",
    /* key: Optional String to prefix the CSS variables */
    key: null
  });
