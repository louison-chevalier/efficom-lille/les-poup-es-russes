    import SineWaves from "sine-waves/sine-waves";

    if ($(".wave-canvas")[0]) {

        for (var i = 1; i <= 20; i++) {

            var waves = new SineWaves({
                el: document.getElementById('wave-' + i),

                speed: 2,

                width: function () {
                    return $(window).width();
                },

                height: function () {
                    return $(window).height();
                },

                ease: 'SineInOut',

                wavesWidth: '100%',

                waves: [{
                    timeModifier: 1,
                    lineWidth: 1,
                    amplitude: -200,
                    wavelength: 100
                }],
                rotate: i * 2,

                // Called on window resize
                resizeEvent: function () {
                    var gradient = this.ctx.createLinearGradient(0, 0, this.width, 0);
                    gradient.addColorStop(0, "rgba(209, 209, 209, 0.4)");
                    gradient.addColorStop(0.5, "rgba(236, 236, 236, 0.8)");
                    gradient.addColorStop(1, "rgba(109, 109, 109, 0.4)");

                    var index = -1;
                    var length = this.waves.length;
                    while (++index < length) {
                        this.waves[index].strokeStyle = gradient;
                    }

                    // Clean Up
                    index = void 0;
                    length = void 0;
                    gradient = void 0;
                }
            });
        }
    }
