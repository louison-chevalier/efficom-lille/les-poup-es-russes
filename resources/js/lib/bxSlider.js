import 'bxslider/dist/jquery.bxslider.min.js';
import 'bxslider/dist/jquery.bxslider.css';

$('.ticker-slider').bxSlider({
    minSlides: 1,
    maxSlides: 3,
    slideMargin: 0,
    ticker: true,
    speed: 60000
});