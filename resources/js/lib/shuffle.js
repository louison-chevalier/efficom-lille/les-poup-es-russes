import Shuffle from 'shufflejs';

if ($("#article-grid")[0]) {

class Trier {
    constructor(element) {
      this.element = element;
      this.shuffle = new Shuffle(element, {
        itemSelector: '.article-preview',
      });
  
      // Log events.
      this.addShuffleEventListeners();
      this._activeFilters = [];
      this.addFilterButtons();
    }

    addShuffleEventListeners() {
        this.shuffle.on(Shuffle.EventType.LAYOUT, (data) => {
          console.log('layout. data:', data);
        });
        this.shuffle.on(Shuffle.EventType.REMOVED, (data) => {
          console.log('removed. data:', data);
        });
      }

      addFilterButtons() {
        const options = document.querySelector('.project-filter');
        if (!options) {
          return;
        }

        const filterButtons = Array.from(options.children);
        const onClick = this._handleFilterClick.bind(this);

        filterButtons.forEach((button) => {
          button.addEventListener('click', onClick, false);
        });
      }

      _handleFilterClick(evt) {
        const btn = evt.currentTarget;
        const isActive = btn.classList.contains('selected');
        const btnGroup = btn.getAttribute('data-group');
        console.log(btn);
        console.log(btnGroup);

        this._removeActiveClassFromChildren(btn.parentNode);
        
        let filterGroup;
        if (isActive) {
          btn.classList.remove('selected');
          filterGroup = Shuffle.ALL_ITEMS;
        } else {
          btn.classList.add('selected');
          filterGroup = btnGroup;
        }
        
        this.shuffle.filter(filterGroup);
        console.log(filterGroup);
      }

      _removeActiveClassFromChildren(parent) {
        const { children } = parent;
        for (let i = children.length - 1; i >= 0; i--) {
          children[i].classList.remove('selected');
        }
      }
}

document.addEventListener('DOMContentLoaded', () => {
    window.demo = new Trier(document.getElementById('article-grid'));
  });
}