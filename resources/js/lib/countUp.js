import {
    CountUp
} from 'countup.js';


function isScrolledIntoView(el) {
    var elemTop = el.getBoundingClientRect().top;
    var elemBottom = el.getBoundingClientRect().bottom;

    var isVisible = (elemTop >= 0) && (elemBottom <= window.innerHeight);
    return isVisible;
}

//---- REVEAL
if ($("#number")[0]) {

    $(window).on('scroll', function () {
        if (isScrolledIntoView(document.getElementById('number'))) {
            var countUp = new CountUp('01', 20);
            var countUp2 = new CountUp('52', 52);
            var countUp3 = new CountUp('65', 65);
            var countUp4 = new CountUp('03', 3);
            countUp.start();
            countUp2.start();
            countUp3.start();
            countUp4.start();

            // Unbind scroll event
            $(window).off('scroll');
        }
    });
}
