import AOS from 'aos';
import 'aos/dist/aos.css'; // You can also use <link> for styles
// ..

let distance_AOS_mobile = "100";
const mq = window.matchMedia("(max-width: 768px)");
if (mq.matches) {
    distance_AOS_mobile = "100";
} else {
    distance_AOS_mobile = "100";
}


AOS.init({
    once: false, // whether animation should happen only once - while scrolling down
    offset: distance_AOS_mobile, // offset (in px) from the original trigger point
    duration: 400, // values from 0 to 3000, with step 50ms
});
