
let $cursor = document.querySelector('.cursor');

function moveCursor(e) {
    jQuery($cursor).addClass('is-moving');
    jQuery($cursor).css({"top": e.pageY, "left": e.pageX});

  clearTimeout(timer2);

   var timer2 = setTimeout(function() {
    jQuery($cursor).removeClass('is-moving');
   }, 300);
}

$(window).on('mousemove', moveCursor);

//--HOVER EFFECTS 

$("a").each(function(){
  $(this).hover(function(){
    $('.cursor').addClass('link');
    }, function(){
    $('.cursor').removeClass('link');
  });
});
