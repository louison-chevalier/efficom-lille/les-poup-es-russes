$(window).on('load', function () {
    setTimeout(function () {
        $('.cookie').css('bottom', '30px');
    }, 2000);
});

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

//-- COOKIE GDPR

$(".cookie-btn-validate").click(function () {
    $('.cookie').css('bottom', '-300px');
    setTimeout(function () {
        $('.cookie').css('display', 'none');
    }, 2000);
    setCookie("Ptf_JulienFovelle", true, 2);
});

$(".cookie__container-close svg").click(function () {
    $('.cookie').css('bottom', '-300px');
    setTimeout(function () {
        $('.cookie').css('display', 'none');
    }, 2000);
});

let cookieGDPR = getCookie("Ptf_JulienFovelle");
if (cookieGDPR == "true") {
    $('.cookie').css('display', 'none');
}

//-- COOKIE LOADER

setTimeout(function () {
    setCookie("Jfovelle_Website_Loader", true, .5);
}, 5000);

let cookieLoader = getCookie("Jfovelle_Website_Loader");
if (cookieLoader == "true") {
    $('.loader').css('display', 'none');
}
