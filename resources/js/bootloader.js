import { cursor } from './cursor.js';
import { event } from './events.js';
import { app } from './app.js';
import { cookie } from './cookie.js';

//-- LIBS
import { aos } from './lib/aos.js';
import { countUp } from './lib/countUp.js';
import { slice } from './lib/slice.js';
import { magnet } from './lib/tween_magnet';
import { scrollOut } from './lib/scrollOut';
import { bxSlider } from './lib/bxSlider';
import { shuffle } from './lib/shuffle';
import { animate } from './lib/animate';
import { morphist } from './lib/morphist';
import { prism } from './lib/prism';
import { loader } from './lib/loader.js';

//-- SPECIFICS LOAD 
import { wave } from './lib/wave';
import { smoothWheel } from './lib/smoothWheel';


