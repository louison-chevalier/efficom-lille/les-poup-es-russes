//--Smoth scroll
window.addEventListener('touchmove', event => {}, {
    passive: true
});

//--HOME CLICK PROJECT

const homeProjectInfo = {
    "project": [{
            "id": 0,
            "image": "url('img/Kosnos.png')",
            "name": "Agence Digitale Kosnos.",
            "desc": "Elaboration d'une agence digitale. -",
            "specs": "UI/UX & Dev. Web",
            "link": "#",
        }, {
            "id": 1,
            "image": "url('img/Mishmash.png')",
            "name": "Application MishMash.",
            "desc": "Concevoir une application mobile. -",
            "specs": "UI/UX",
            "link": "#",
        }, {
            "id": 2,
            "image": "url('img/Metasequoia.png')",
            "name": "Elageur MetaSequoia.",
            "desc": "Un site internet à échelle locale. -",
            "specs": "UI/UX & Dev. Web",
            "link": "#",
        }, {
            "id": 3,
            "image": "url('img/Marathon.png')",
            "name": "Marathons du Web 2018/2019.",
            "desc": "Réalisation d'un site en 36h. -",
            "specs": "Webdesign & Dev. Web",
            "link": "#",
        },

    ]
}

let projectNumber = 0;
fillFields();

function fillFields() {
    $('#img-home').css('background-image', homeProjectInfo.project[projectNumber].image);
    $('#title-home').html(homeProjectInfo.project[projectNumber].name);
    $('#desc-home').html(homeProjectInfo.project[projectNumber].desc);
    $('#specs-home').html(homeProjectInfo.project[projectNumber].specs);
}

//-- LEFT
$('.arrow-left').click(function homeProjectLeft () {
    if (projectNumber != 0) {
        projectNumber --;
    }

    getOut();
    setTimeout(function () {
        fillFields()
        getIn();
    }, 1000);

});

//-- RIGHT
$('.arrow-right').click(function homeProjectRight() {
    if (projectNumber != (Object.keys(homeProjectInfo.project).length)-1) {
        projectNumber ++;
    }

    getOut();
    setTimeout(function () {
        fillFields()
        getIn();
    }, 1000);


});

function getOut() {
    const titleImg =  document.getElementById('img-home');
    titleImg.classList.add('animated', 'fadeOutLeft');

    const titleHome =  document.getElementById('title-home');
    titleHome.classList.add('animated', 'fadeOutDown');
    
    const titleDesc =  document.getElementById('desc-home');
    titleDesc.classList.add('animated', 'fadeOutDown');

    const titleSpecs =  document.getElementById('specs-home');
    titleSpecs.classList.add('animated', 'fadeOutDown');

}
function getIn() {
    const titleImg =  document.getElementById('img-home');
    titleImg.classList.remove('animated', 'fadeOutLeft');
    titleImg.classList.add('animated', 'fadeInRight');

    const titleHome =  document.getElementById('title-home');
    titleHome.classList.remove('animated', 'fadeOutDown');
    titleHome.classList.add('animated', 'fadeInUp');

    const titleDesc =  document.getElementById('desc-home');
    titleDesc.classList.remove('animated', 'fadeOutDown');
    titleDesc.classList.add('animated', 'fadeInUp');

    const titleSpecs =  document.getElementById('specs-home');
    titleSpecs.classList.remove('animated', 'fadeOutDown');
    titleSpecs.classList.add('animated', 'fadeInUp');

}