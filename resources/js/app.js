//-- ADD DATA

$("a").each(function () {
    $(this).attr('data-cursor', '');
    $(this).attr('data-color', '');
});


//-- SmoothWheel     



//---- Scroll DOWN
$(window).scroll(function () {
    if ($(this).scrollTop() >= 50) {
        $('.aside-scroll__content').addClass('hidden');
    } else {
        $('.aside-scroll__content').removeClass('hidden');
    }
});

$(window).scroll(function () {
    if ($(this).scrollTop() >= 650) {
        $('.wave-canvas').addClass('disabled');
    } else {
        $('.wave-canvas').removeClass('disabled');
    }
});

$('.aside-scroll').click(function () {
    $('body,html').animate({
        scrollTop: 1000
    }, 500);
});

//---- SCROLL INDICATOR

// capture scroll
$(window).scroll(function () {
    var top = $(window).scrollTop(),
        docHeight =

        $(document).height(),
        winHeight = $(window).height();
    var scroll = (top / (docHeight - winHeight)) * 100;

    $('.scroll').css('width', (scroll + '%'));
});

//---- MENU

$('.menu-burger').on('click', function () {
    $(this).toggleClass('btn-active');
    $('.menu').toggleClass('is-visible');
    $('.cursor').toggleClass('cursor--menu');
    $('.aside-logo__content a').toggleClass('logo-active');
});

//---- DATA COLOR ARTICLE 

let dataYellow = "#fecb61"; //UI
let dataGreen = "#fecb61"; //UX
let dataBlue = "#174ffc"; //DEV
let dataRed = "#ff565c"; //ARTICLES
let dataPurple = "#8393ca"; //MADE

let dataColor = "";

let article = document.querySelector('.article-header__info');

if (article != null) {
    if (article.dataset.groups == '["developpement web"]') {
        dataColor = dataBlue;
    }

    if (article.dataset.groups == '["design ui"]') {
        dataColor = dataYellow;
    }

    $('.article-header__info').css('color', dataColor);
    $('strong').css('color', dataColor);
    $('.scroll').css('background-color', dataColor);
}

//-- ARROW HOME

$('.arrow').on('click touch', function(e) {

    e.preventDefault();

    let arrow = $(this);

    if(!arrow.hasClass('animate')) {
        arrow.addClass('animate');
        setTimeout(() => {
            arrow.removeClass('animate');
        }, 1600);
    }

});