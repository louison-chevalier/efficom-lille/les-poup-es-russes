const mix = require('laravel-mix');
require('laravel-mix-purgecss');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |npm run dev
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.options({
    clearConsole: true, // in watch mode, clears console after every build
    postCss: [
        require('autoprefixer')({
            browsers: ['last 40 versions'],
            cascade: false
        })
    ]
});

mix.js('resources/js/bootloader.js', 'public/js/main.js')
    .sass('resources/sass/common.scss', 'public/css/app.css', {
        includePaths: [path.resolve(__dirname, 'node_modules')]
    })
    .purgeCss({
        enabled: true,
    })
    .browserSync('http://127.0.0.1:8000/');
