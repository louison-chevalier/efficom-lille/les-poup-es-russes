i# JulienFovelle v1.0.0 

> How this project work and use Scss files and Minify JavaScript files with Grunt Webpack.


## Getting Started

If you haven't used [Grunt](https://gruntjs.com/) before, be sure to check out the [Getting Started](https://gruntjs.com/getting-started) guide, as it explains how to create a [Gruntfile](https://gruntjs.com/sample-gruntfile) as well as install and use Grunt plugins. Once you're familiar with that process, you may install this plugin with this command:

Cheeck if npm & composer is installed 

```shell
$ npm -v
```
or install 

```shell
$ npm install
$ npm install -g grunt-cli
```

```shell
$ composer install
```

Required Webpack, grunt and npm.

# Basic commands

For the execution of a single compilation

```shell
$ npm run compile
```

To run a continuous compilation 'watcher'

```shell
$ npm run watch
```

Install Module

```shell
$ npm install --save <module>
```

## License
COPYRIGHT © [Fovelle Julien](https://julienfovelle.fr/)