<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use App\Article;
use App\Projet;
use App\Librairy;

use Lionix\SeoManager\Facades\SeoManager;
use Lionix\SeoManager\Traits\Appends;


class Controller extends BaseController
{
    public function home(){
        return view('pages.layout-home');    
    }
}
